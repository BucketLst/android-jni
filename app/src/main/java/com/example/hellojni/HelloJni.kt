package com.example.hellojni

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity



class HelloJni : AppCompatActivity() {

    private external fun stringFromJNI(): String

    init {
        System.loadLibrary("hello-jni")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val tv = TextView(this)
        tv.text = stringFromJNI()
        setContentView(tv);
    }

}

